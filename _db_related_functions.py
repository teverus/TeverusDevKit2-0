import os
import sqlite3

from _constants import DATABASE_PATH, DATABASE_NAME


def set_up_a_connection(database_name=DATABASE_NAME, database_path=DATABASE_PATH):
    """
    Sets up a connection to a database.
    If the latter doesn't exist - it creates it.
    Returns connection and cursor
    """
    connection = sqlite3.connect(os.path.join(database_path, f"{database_name}.db"))
    return connection, connection.cursor()


def create_a_table(table_name: str, columns: dict, database_name=DATABASE_NAME, database_path=DATABASE_PATH):
    """
    Creates a table with a specific name and any number of columns.
    You can also set a database name and create it with this function.
    """
    connection, cursor = set_up_a_connection(database_name, database_path)

    table_columns = ""
    for index, name in enumerate(columns):
        table_columns += f"{name} {columns[name]}{', ' if index + 1 != len(columns) else ''}"

    with connection:
        cursor.execute(f"create table {table_name} ({table_columns})")
