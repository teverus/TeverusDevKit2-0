import os

DATABASE_NAME = "Default database name"
DATABASE_PATH = os.getcwd()


class DataType:
    TEXT = "text"
    INT = "int"
